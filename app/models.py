from sqlalchemy import Column, ForeignKey, Integer, String, Boolean,MetaData
from sqlalchemy.sql.sqltypes import TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.orm import relationship, declarative_base
# from sqlalchemy.ext.declarative import declarative_base


# The base class will be the base for all the models we'll create.
Base = declarative_base()


####################
#### ORM Models ####
####################


# First class/model/Table in database
class User (Base):
    __tablename__ = "User" #table name in postgres

    id = Column(Integer, primary_key=True, nullable=False, index=True)
    username = Column(String, nullable=False)
    email = Column(String, nullable=False)
    password = Column(String, nullable=False)
    created_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text('now()'))


# meal plan table in database
class MealPlan (Base):
    __tablename__ = "MealPlan" #table name in postgres

    id = Column(Integer, primary_key=True, nullable=False, index=True)
    name = Column(String(255), nullable=False)
    description = Column(String, nullable=True)
    diet= Column(String, nullable=False)
    created_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text('now()'))
    user_id = Column(Integer, ForeignKey("User.id", ondelete="CASCADE"), nullable=False)
    writer = relationship("User")
    # target_calories = Column(Integer, nullable=False)


# recipes table in database
class Recipe(Base):
    __tablename__ = 'Recipe'
    
    id = Column(Integer, primary_key=True)
    title = Column(String)
    instructions = Column(String, nullable=True)


# Ingredients table in database
class Ingredients(Base):
    __tablename__ = 'Ingredients'
    
    id = Column(Integer, primary_key=True)
    name = Column(String)


# Link_ingredients_recipe table in database
class Link_ingredients_recipe(Base):
    __tablename__ = "Link_ingredients_recipe"

    ingredient_id = Column(Integer, ForeignKey("Ingredients.id"), primary_key=True, nullable=False)
    ingredient = relationship("Ingredients")
    recipe_id = Column(Integer, ForeignKey("Recipe.id"), primary_key=True, nullable=False)
    recipe = relationship("Recipe")


# Link_meal_recipe table in database
class Link_meal_recipe(Base):
    __tablename__ = "Link_meal_recipe"

    mealplan_id = Column(Integer, ForeignKey("MealPlan.id"), primary_key=True, nullable=False)
    mealplan = relationship("MealPlan")
    recipe_id = Column(Integer, ForeignKey("Recipe.id"), primary_key=True, nullable=False)
    recipe = relationship("Recipe")
