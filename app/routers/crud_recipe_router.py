from fastapi import Depends, FastAPI, Body, Response, status, HTTPException, APIRouter
from pydantic import BaseModel
from typing import Optional, List
from .. import models, schemas #. means current folder
from ..database import database_engine, get_db  #. means current folder
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from ..utilities import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
import requests
from sqlalchemy import create_engine, Column, Integer, String, func
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import declarative_base
from ..config import settings
from ..models import Recipe, Ingredients, Link_ingredients_recipe
from ..utilities import recipe_manager


router = APIRouter(
    prefix="/recipes",
    tags=["recipes"]
)

# Create a SQLAlchemy engine to connect to the database
# database_engine =f"postgresql://{settings.database_username}:{settings.database_password}@{settings.database_host}/{settings.database_name}"




############### recipes ENDPOINTS ####################





# GET recipes with specicic input ingrédients from the user (ex = patatos)
# This ENPOINT should enable the user to GET from our Static DB, all recipe with the "KEY WORD" he inputed
# the OUTPUT/RESPONSE are all the recipies that contain the recipe_ingredients

@router.post('/')
def get_recipes(recipe_ingredients: schemas.Ingredients, db: Session = Depends(get_db), user_id : int = Depends(jwt_manager.decode_token)):

    fetched_recipes= recipe_manager.import_recipes(recipe_ingredients.ingredients)

    ############################ store recipes & Ingredients in db  ###########################
    for recipe in fetched_recipes:
        # Create a new recipe model instance
        new_recipe = Recipe(id=recipe['id'], title=recipe['title'], instructions=None)

        new_recipe.instructions = recipe_manager.import_recipe_instructions(recipe['id'])
        
        try:
            # Add the recipe to the db
            db.add(new_recipe)
            # Commit the changes to the database
            db.commit()
        except IntegrityError:
            # If the recipe already exists in the database, rollback the session
            db.rollback()

        # Create instances of the model ingredients used in the recipe and link them to the recipe
        for used_ingredient in recipe['usedIngredients']:
            ingredient = db.query(models.Ingredients).filter_by(id=used_ingredient['id']).first()
            if not ingredient:
                # If the ingredient is not in the database yet, create a new instance
                ingredient = Ingredients(id=used_ingredient['id'], name=used_ingredient['name'])
                try:
                    db.add(ingredient)
                    db.commit()
                except IntegrityError:
                    db.rollback()
            # Link the ingredient to the recipe
            link = Link_ingredients_recipe(ingredient=ingredient, recipe=new_recipe)
            try:
                db.add(link)
                db.commit()
            except IntegrityError:
                db.rollback()


    ############################ Query the database for all the stored recipes and their linked ingredients  ###########################
    # Query the database for all the stored recipes and their linked ingredients
    # stored_recipes = db.query(models.Recipe).all()
    # Query the database for the stored recipes that contain all the provided ingredients
    ingredients_list = recipe_ingredients.ingredients.split(",")

    stored_recipes = db.query(models.Recipe).join(models.Link_ingredients_recipe).join(models.Ingredients).filter(models.Ingredients.name.in_(ingredients_list)).all()
    
    print(f"Number of stored recipes: {len(stored_recipes)}")

    print(f"Ingredients list: {ingredients_list}")

    # Format the response data as a dictionary
    data = []
    for recipe in stored_recipes:
        recipe_data = {
            'id': recipe.id,
            'title': recipe.title,
            'instructions': recipe.instructions,
            'usedIngredients': []
        }
        links = db.query(models.Link_ingredients_recipe).filter_by(recipe_id=recipe.id).all()
        for link in links:
            recipe_data['usedIngredients'].append({'id': link.ingredient.id, 'name': link.ingredient.name})
        data.append(recipe_data)


    return {"data": data}


# GET recipes with ID that belongs to this specific User. 
# the OUTPUT from this ENPOINTS shall not be stored in our static DB, but just displayed to the user

@router.get('/{user_id}', response_model=schemas.Recipes_Response)
def get_recipes(uri_id: int, db: Session = Depends(get_db), user_id : int = Depends(jwt_manager.decode_token)):
     corresponding_recipes = db.query(models.Recipe).filter(
         models.Recipe.id == uri_id).first()
     if not corresponding_recipes:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding recipe was found with id:{uri_id}"
         )
     return corresponding_recipes