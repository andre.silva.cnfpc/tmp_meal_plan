from fastapi import Depends, FastAPI, Body, Response, status, HTTPException, APIRouter
from pydantic import BaseModel
from typing import Optional, List
from .. import models, schemas #. means current folder
from ..database import database_engine, get_db  #. means current folder
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from ..utilities import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm


router = APIRouter(
    prefix="/users",
    tags=["users"]
)


############### USERS ENDPOINTS ####################

# CREATE A NEW USER ENPPOINT : this enpoints will enable the user/consumer to create his account our our API and be an Authorized user
# this step is needed to do any operation on our secured Enpoints

@router.post('/', status_code=status.HTTP_201_CREATED, response_model=schemas.User_Response)
def create_user(user_body: schemas.User, db: Session = Depends(get_db)):
    # hashing the password
    pwd_hashed = hash_manager.hash_pass(user_body.password)
    user_body.password = pwd_hashed # override password in the body by hashed password 
    new_user = models.User(**user_body.dict()) #** pass to the model builder from SQLAlchemy the serialize of the pydantic model
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user



# DELETE  AN existing USER  ENDPOINT : this enpoints will enable the EXISTING user/consumer to DELETE the account created on our our API
# it should realy secured as it is a direct access to our DB
# This action shall impact the Mealplans created by this specific user as well (to be discussed)

@router.delete('/{uri_id}', status_code=status.HTTP_204_NO_CONTENT)
def delete_user(uri_id: int, db: Session = Depends(get_db), user_id : int = Depends(jwt_manager.decode_token)):
    # find user with id provided             
    query_user = db.query(models.User).filter(
        models.User.id == uri_id)
    if not query_user.first():  # Check if user exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding user was found with id:{uri_id}"
        )
    query_user.delete()  # DELETE the user
    db.commit()  # Save changes to the DB
    return Response(status_code=status.HTTP_204_NO_CONTENT)


# UPDATE USERS ENDPPOINT : this enpoints will enable the EXISTING user/consumer to UPDATE an account created on our our API
# it should realy secured as it is a direct access to our DB
# these UPDATE will concern datas like (password and Email), but not the Usermane, to keep thing simple to handle

@router.put('/{uri_id}', response_model=schemas.User_Response)
def update_user(uri_id: int, user_body: schemas.User, db: Session = Depends(get_db), user_id : int = Depends(jwt_manager.decode_token)):
    # find user with id provided                
    query_user = db.query(models.User).filter(models.User.id == uri_id)
    if not query_user.first():  # make sure it exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding user was found with id:{uri_id}"
        )
    query_user.update(user_body.dict())  # Update the user
    db.commit()  # Save changes to DB
    return query_user.first()
