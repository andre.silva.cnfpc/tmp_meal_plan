from fastapi import Depends, FastAPI, Body, Response, status, HTTPException, APIRouter
from pydantic import BaseModel
from typing import Optional, List
from .. import models, schemas #. means current folder
from ..database import database_engine, get_db  #. means current folder
from sqlalchemy.orm import Session
from ..utilities import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm


router = APIRouter(
    prefix="/auth",
    tags=["auth"]
)

incorrect_exeption=HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Incorrect username or password",
    headers={"WWW-Authenticate":"Bearer"}
    )

# Authentification router : this router enable the user to authenticate and interacte with all the secured ENPOINT. 
# He basically create (POST method) his profil on our DB first (username, email, password) and then autenticate to be able to acess to the ENPOINTS
@router.post('/',response_model=schemas.Token)
def auth_user(user_credentials : OAuth2PasswordRequestForm =Depends(), db: Session = Depends(get_db)):
    corresponding_user = db.query(models.User).filter(models.User.email == user_credentials.username).first()
    if not corresponding_user:
        raise incorrect_exeption
    pass_valid = hash_manager.verify_password(user_credentials.password, corresponding_user.password)

    if not pass_valid:
        raise incorrect_exeption
    
    jwt = jwt_manager.generate_token(corresponding_user.id)


    return jwt # return the generated Token
