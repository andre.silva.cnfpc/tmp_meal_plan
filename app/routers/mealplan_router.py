from fastapi import Depends, FastAPI, Body, Response, status, HTTPException, APIRouter
from pydantic import BaseModel
from typing import Optional, List
from .. import models, schemas #. means current folder
from ..database import database_engine, get_db  #. means current folder
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from ..utilities import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm


router = APIRouter(
    prefix="/MealPlan",
    tags=["MealPlan"]
)


############### MEALPLAN ENDPOINTS ####################

# Create(POST method) a new MealPlan. 
# This ENPOINt shloud enable the authorize user, the create his own mealplan for the day or week.
# This Mealplan can be composed of the recipes he searches and Get from our static BD with a specific KEY WORD (ex = Vegan)
# this Mealplan can have a calories target (OPTIONNAL in our application if we have time)
#  The output will be store in our static DB inside the MealPlan table and linked to a secific user (the one that created it)

@router.post('/', status_code=status.HTTP_201_CREATED, response_model=schemas.MealPlan_Response)
def create_MealPlan(meal_plan_body: schemas.MealPlan, db: Session = Depends(get_db), user_id : int = Depends(jwt_manager.decode_token)):
    meal_plan = models.MealPlan(**meal_plan_body.dict()) #** pass to the model builder from SQLAlchemy the serialize of the pydantic model
    db.add(meal_plan)
    db.commit()
    db.refresh(meal_plan)
    return meal_plan


# GET all the MealPlans endpoint. This ENPOINT shall enable the user to GET all the MealPlans he created inside our API
@router.get('/', response_model=List[schemas.MealPlan_Response])
def get_MealPlan(db: Session = Depends(get_db), user_id : int = Depends(jwt_manager.decode_token)):
    all_MealPlan = db.query(models.User).all()
    return all_MealPlan



# DELETE MealPlan ENPOINT
# This ENPOINT will enable the Authozided User to DELETE one specific Meal plan he created on our API/static database
@router.delete('/{uri_id}', status_code=status.HTTP_204_NO_CONTENT)
def delete_user(uri_id: int, db: Session = Depends(get_db), user_id : int = Depends(jwt_manager.decode_token)):
    # find user with id provided             
    query_user = db.query(models.User).filter(
        models.User.id == uri_id)
    if not query_user.first():  # Check if user exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding user was found with id:{uri_id}"
        )
    query_user.delete()  # DELETE the user
    db.commit()  # Save changes to the DB
    return Response(status_code=status.HTTP_204_NO_CONTENT)



# UPDATE (PUT method) meal_plan endpoint. 
# This ENPOINT shall enable the authorized user to UPDATE One Meal plan, that he created and stored on our static DB
# This specific meal plan can be found by it ID our our DB
# The changes need to be stored inside our static DB 

@router.put('/{uri_id}', response_model=schemas.MealPlan_Response)
def update_user(uri_id: int, meal_plan_body: schemas.MealPlan, db: Session = Depends(get_db), user_id : int = Depends(jwt_manager.decode_token)):
    # find meal_plan with id provided                
    update_meal_plan = db.query(models.MealPlan).filter(models.MealPlan.id == uri_id)
    if not update_meal_plan.first():  # make sure it exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding user was found with id:{uri_id}"
        )
    update_meal_plan.update(meal_plan_body.dict())  # Update the meal_plan
    db.commit()  # Save changes to DB
    return update_meal_plan.first()
