from datetime import datetime
from pydantic import BaseModel, EmailStr
from typing import Optional

# Pydantic schema for User Body validation (REQUEST)
class User(BaseModel):
    email: EmailStr  
    password: str
    username:str

class User_credentials(User):
    pass 

# Pydantic schema for MealPlan Body validation (REQUEST)
class MealPlan(BaseModel):
    user_id: int
    name: str
    description: Optional[str] = None
    diet:Optional[str]=None


# Pydantic schema for MealPlanRecip Body validation (REQUEST)
class RecipeCreate(BaseModel):
    title: str
    ingredients: str
    instructions: Optional[str] = None


class Ingredients(BaseModel):
    ingredients : str


# Pydantic schema User Response (RESPONSE)
class User_Response(BaseModel):
    id: int
    created_at: datetime
    class Config:  # Important for pydantic model/schema translation
        orm_mode = True

# Pydantic schema MealPlan Response (RESPONSE)
class MealPlan_Response(BaseModel):
    id:int
    created_at: datetime
    writer: User_Response
    class Config:  # Important for pydantic model/schema translation
        orm_mode = True

 #  Pydantic schema MealPlanRecipe Response (RESPONSE)
class Recipes_Response(BaseModel):
    id: int
    created_at: datetime
    class Config:  # Important for pydantic model/schema translation
        orm_mode = True

class Token(BaseModel):
    access_token : str
    token_type: str

