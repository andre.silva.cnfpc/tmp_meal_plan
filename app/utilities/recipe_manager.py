from fastapi import Depends, FastAPI, Body, Response, status, HTTPException, APIRouter
from pydantic import BaseModel
from typing import Optional, List
from .. import models, schemas #. means current folder
from ..database import database_engine, get_db  #. means current folder
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from ..utilities import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
import requests
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import declarative_base
from ..config import settings
from ..models import Recipe, Base, Ingredients,Link_ingredients_recipe
from ..utilities import recipe_manager
# GET recipes from spoonacular and populate our DB (static BD/storage). 
# Is this Enpoints available for the user or in the background ?


def import_recipes(ingredients):
    api_key = '55a7b0d60fc9489f816cecbf37ecb512'  # Replace with your Spoonacular API key
    # ingredients = 'chicken, broccoli, potatoes'  # Replace with your desired ingredients
    # ingredients = ",".join(ingredients)
    url = f'https://api.spoonacular.com/recipes/findByIngredients?apiKey={api_key}&ingredients={ingredients}&number=10'
    print(url)
    
    # Retrieve the recipes from the Spoonacular API
    response = requests.get(url)
    recipes = response.json()
    
    return recipes


def import_recipe_instructions(recipe_id):
    try:
        url = f"https://api.spoonacular.com/recipes/{recipe_id}/analyzedInstructions?apiKey=55a7b0d60fc9489f816cecbf37ecb512"
        print(url)

        # Retrieve Spoonacular API
        response = requests.get(url)

        response.raise_for_status()
        instructions = ""
        for step in response.json()[0]["steps"]:
            instructions += step["step"] + "\n"
    except requests.exceptions.RequestException as e:
        print(f"Failed to fetch instructions for recipe {recipe_id}: {e}")
    
    return instructions